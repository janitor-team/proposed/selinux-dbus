Source: selinux-dbus
VCS-Git: https://salsa.debian.org/selinux-team/selinux-dbus.git
VCS-Browser: https://salsa.debian.org/selinux-team/selinux-dbus
Priority: optional
Section: utils
Maintainer: Debian SELinux maintainers <selinux-devel@lists.alioth.debian.org>
Uploaders: Laurent Bigonville <bigon@debian.org>,
           Russell Coker <russell@coker.com.au>
Standards-Version: 4.5.0
Build-Depends: debhelper (>= 12),
               dh-python,
               python3 (>= 3.2)
Homepage: http://userspace.selinuxproject.org/
Rules-Requires-Root: no

Package: policycoreutils-dbus
Architecture: all
Depends: dbus (>= 1.8),
         policycoreutils (>= 3.1),
         policycoreutils-python-utils (>= 3.1),
         policykit-1,
         python3-dbus,
         python3-gi,
         python3-selinux (>= 3.1),
         python3-slip-dbus,
         ${misc:Depends},
         ${python3:Depends}
Breaks: policycoreutils-gui (<< 2.7~)
Replaces: policycoreutils-gui (<< 2.7~)
Description: SELinux core policy utilities (D-Bus daemon)
 Security-enhanced Linux is a patch of the Linux® kernel and a number
 of utilities with enhanced security functionality designed to add
 mandatory access controls to Linux.  The Security-enhanced Linux
 kernel contains new architectural components originally developed to
 improve the security of the Flask operating system. These
 architectural components provide general support for the enforcement
 of many kinds of mandatory access control policies, including those
 based on the concepts of Type Enforcement®, Role-based Access Control,
 and Multi-level Security.
 .
 This package contains the org.selinux D-Bus daemon.
